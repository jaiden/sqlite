# Sheep's SQLite3 for Go

Very limited SQLite for Go for my own use and learning CGo.
Handcrafted with love and more footguns.

Basically a less powerful [`crawshaw/sqlite`](https://pkg.go.dev/crawshaw.io/sqlite) that also doesn't vendor source.

- No dependencies!
- Non-vendored SQLite only
- Still not a `database/sql` driver!
