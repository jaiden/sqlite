package sqlite

import (
	"context"
	"sync"
)

// Pool is a pool of SQLite connections.
//
// It allows for a single writer and multiple readers.
// It is safe for use by multiple goroutines.
//
// It automatically sets WAL and thus is not suitable for in-memory databases.
type Pool struct {
	writeBorrower chan *Database
	readPool      chan *Database
	closed        chan struct{}

	writeDatabase *Database

	mu  sync.RWMutex
	all map[*Database]context.CancelFunc
}

func OpenPool(path string, readSize uint) (*Pool, error) {
	if path == ":memory:" {
		return nil, ErrUnknown
	}

	db, err := OpenDatabase(path, OpenReadWrite|OpenCreate|OpenURI|OpenNoMutex)
	if err != nil {
		return nil, err
	}

	if err := db.ExecuteTransient("PRAGMA journal_mode = WAL"); err != nil {
		return nil, err
	}

	if err := db.ExecuteTransient("PRAGMA synchronous = NORMAL"); err != nil {
		return nil, err
	}

	if err := db.ExecuteTransient("PRAGMA busy_timeout = 5000"); err != nil {
		return nil, err
	}

	p := &Pool{
		writeBorrower: make(chan *Database, 1),
		readPool:      make(chan *Database, readSize),
		closed:        make(chan struct{}),
		writeDatabase: db,
		all:           make(map[*Database]context.CancelFunc, readSize+1),
	}

	p.all[db] = func() {}

	p.writeBorrower <- db

	for range readSize {
		db, err := OpenDatabase(path, OpenReadOnly|OpenURI|OpenNoMutex)

		if err != nil {
			for db := range p.all {
				db.Close()
			}

			return nil, err
		}

		p.all[db] = func() {}

		p.readPool <- db
	}

	return p, nil
}

func (p *Pool) GetWrite(ctx context.Context) *Database {
	if ctx == nil {
		ctx = context.Background()
	}

	ctx, cancel := context.WithCancel(ctx)

	select {
	case conn := <-p.writeBorrower:
		p.mu.Lock()
		p.all[conn] = cancel
		p.mu.Unlock()

		return conn
	case <-ctx.Done():
	case <-p.closed:
	}

	cancel()

	return nil
}

func (p *Pool) GetRead(ctx context.Context) *Database {
	if ctx == nil {
		ctx = context.Background()
	}

	ctx, cancel := context.WithCancel(ctx)

	select {
	case db := <-p.readPool:
		p.mu.Lock()
		p.all[db] = cancel
		p.mu.Unlock()

		return db
	case <-ctx.Done():
	case <-p.closed:
	}

	cancel()

	return nil
}

func (p *Pool) PutWrite(db *Database) {
	if db != p.writeDatabase {
		panic("not my db")
	}

	p.mu.RLock()
	cancel := p.all[db]
	p.mu.RUnlock()

	cancel()

	p.writeBorrower <- db
}

func (p *Pool) PutRead(db *Database) {
	p.mu.RLock()
	cancel, found := p.all[db]
	p.mu.RUnlock()

	if !found {
		panic("not found :(")
	}

	cancel()

	p.readPool <- db
}

func (p *Pool) Close() error {
	p.mu.RLock()
	for _, cancel := range p.all {
		cancel()
	}
	p.mu.RUnlock()

	return nil
}
