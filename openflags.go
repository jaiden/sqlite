package sqlite

// [Flags For File Open Operations]
//
// [Flags For File Open Operations]: https://www.sqlite.org/c3ref/c_open_autoproxy.html
const (
	OpenReadOnly      = 0x00000001 /* Ok for sqlite3_open_v2() */
	OpenReadWrite     = 0x00000002 /* Ok for sqlite3_open_v2() */
	OpenCreate        = 0x00000004 /* Ok for sqlite3_open_v2() */
	OpenDeleteOnClose = 0x00000008 /* VFS only */
	OpenExclusive     = 0x00000010 /* VFS only */
	OpenAutoproxy     = 0x00000020 /* VFS only */
	OpenURI           = 0x00000040 /* Ok for sqlite3_open_v2() */
	OpenMemory        = 0x00000080 /* Ok for sqlite3_open_v2() */
	OpenMainDb        = 0x00000100 /* VFS only */
	OpenTempDb        = 0x00000200 /* VFS only */
	OpenTransientDb   = 0x00000400 /* VFS only */
	OpenMainJournal   = 0x00000800 /* VFS only */
	OpenTempJournal   = 0x00001000 /* VFS only */
	OpenSubJournal    = 0x00002000 /* VFS only */
	OpenSuperJournal  = 0x00004000 /* VFS only */
	OpenNoMutex       = 0x00008000 /* Ok for sqlite3_open_v2() */
	OpenFullMutex     = 0x00010000 /* Ok for sqlite3_open_v2() */
	OpenSharedCache   = 0x00020000 /* Ok for sqlite3_open_v2() */
	OpenPrivateCache  = 0x00040000 /* Ok for sqlite3_open_v2() */
	OpenWAL           = 0x00080000 /* VFS only */
	OpenNoFollow      = 0x01000000 /* Ok for sqlite3_open_v2() */
	OpenExResCode     = 0x02000000 /* Extended result codes */
)
