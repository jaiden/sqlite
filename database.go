package sqlite

// #include <stdint.h>
//
// void free(void *);
//
// typedef struct sqlite3 sqlite3;
// typedef struct sqlite3_stmt sqlite3_stmt;
//
// int sqlite3_open_v2(const char *filename, sqlite3 **ppDb, int flags, const char *zVfs);
// int sqlite3_close_v2(sqlite3 *);
//
// void sqlite3_interrupt(sqlite3 *);
// int sqlite3_is_interrupted(sqlite3 *);
import "C"
import (
	"unsafe"
)

type Database struct {
	db *C.sqlite3

	statementCache map[string]*Statement
}

func OpenDatabase(filename string, flags int) (*Database, error) {
	cFile := C.CString(filename)
	defer C.free(unsafe.Pointer(cFile))

	db := &Database{}

	if r := C.sqlite3_open_v2(cFile, &db.db, C.int(flags), (*C.char)(nil)); r != ResultOk {
		return nil, resultToErr(r)
	}

	return db, nil
}

func (db *Database) Close() error {
	for _, stmt := range db.statementCache {
		stmt.finalize()
	}

	if r := C.sqlite3_close_v2(db.db); r != ResultOk {
		return resultToErr(r)
	}

	return nil
}

func (db *Database) Interrupt() {
	C.sqlite3_interrupt(db.db)
}

func (db *Database) FlushStatements() {
	for _, stmt := range db.statementCache {
		stmt.finalize()
	}

	clear(db.statementCache)
}

func (db *Database) ExecuteTransient(query string) error {
	stmt, err := db.prepareUncached(query, 0x00)
	if err != nil {
		return nil
	}

	if _, err := stmt.Step(); err != nil {
		return err
	}

	if err := stmt.finalize(); err != nil {
		return err
	}

	return nil
}
