package sqlite

// #include <stdint.h>
//
// void free(void *);
//
// typedef struct sqlite3 sqlite3;
// typedef struct sqlite3_stmt sqlite3_stmt;
// typedef void (*sqlite3_destructor_type)(void*);
//
// int sqlite3_prepare_v3(
//  sqlite3 *db,            /* Database handle */
//  const char *zSql,       /* SQL statement, UTF-16 encoded */
//  int nByte,              /* Maximum length of zSql in bytes. */
//  unsigned int prepFlags, /* Zero or more SQLITE_PREPARE_ flags */
//  sqlite3_stmt **ppStmt,  /* OUT: Statement handle */
//  const char **pzTail     /* OUT: Pointer to unused portion of zSql */
// );
//
// int sqlite3_step(sqlite3_stmt *);
//
// int sqlite3_finalize(sqlite3_stmt *pStmt);
//
// int sqlite3_clear_bindings(sqlite3_stmt *);
// int sqlite3_reset(sqlite3_stmt *pStmt);
//
// const char *sqlite3_bind_parameter_name(sqlite3_stmt *, int);
// int sqlite3_bind_parameter_count(sqlite3_stmt *);
// int sqlite3_bind_parameter_index(sqlite3_stmt *, const char *zName);
//
// int sqlite3_bind_double(sqlite3_stmt *, int, double);
// int sqlite3_bind_int64(sqlite3_stmt *, int, int64_t);
// int sqlite3_bind_null(sqlite3_stmt *, int);
// int sqlite3_bind_text64(sqlite3_stmt *, int, const char *, uint64_t, sqlite3_destructor_type, unsigned char);
// int sqlite3_bind_zeroblob64(sqlite3_stmt *, int, uint64_t);
//
// double sqlite3_column_double(sqlite3_stmt *, int);
// int64_t sqlite3_column_int64(sqlite3_stmt *, int);
// const unsigned char *sqlite3_column_text(sqlite3_stmt *, int);
//
// int sqlite3_column_bytes(sqlite3_stmt *, int);
//
// sqlite3_destructor_type mercandev_go_sqlite_free = free;
import "C"
import (
	"unsafe"
)

// Thin wrapper around the sqlite3_stmt object.
type Statement struct {
	stmt *C.sqlite3_stmt
}

func (db *Database) prepareUncached(query string, flags uint) (*Statement, error) {
	stmt := &Statement{}

	cQuery := C.CString(query)
	defer C.free(unsafe.Pointer(cQuery))

	if r := C.sqlite3_prepare_v3(db.db, cQuery, -1, C.uint(flags), &stmt.stmt, (**C.char)(nil)); r != ResultOk {
		return nil, resultToErr(r)
	}

	return stmt, nil
}

func (db *Database) Prepare(query string, flags uint) (*Statement, error) {
	cached := db.statementCache[query]
	if cached != nil {
		return cached, nil
	}

	return db.prepareUncached(query, flags)
}

func (s *Statement) finalize() error {
	return resultToErr(C.sqlite3_finalize(s.stmt))
}

func (s *Statement) ClearBindings() {
	C.sqlite3_clear_bindings(s.stmt)
}

func (s *Statement) Reset() error {
	return resultToErr(C.sqlite3_reset(s.stmt))
}

func (s *Statement) Step() (bool, error) {
	switch r := C.sqlite3_step(s.stmt); r {
	case ResultRow:
		return true, nil
	case ResultDone:
		return false, nil
	default:
		return false, resultToErr(r)
	}
}

func (s *Statement) BindFloat(index int, value float64) error {
	return resultToErr(C.sqlite3_bind_double(s.stmt, C.int(index), C.double(value)))
}

func (s *Statement) BindInt64(index int, value int64) error {
	return resultToErr(C.sqlite3_bind_int64(s.stmt, C.int(index), C.int64_t(value)))
}

// https://www.sqlite.org/c3ref/bind_blob.html
func (s *Statement) BindText(index int, value string) error {
	cValue := C.CString(value)
	length := C.uint64_t(len(value))

	return resultToErr(C.sqlite3_bind_text64(s.stmt, C.int(index), cValue, length, C.mercandev_go_sqlite_free, 1))
}

func (s *Statement) ColumnSize(index int) int {
	return int(C.sqlite3_column_bytes(s.stmt, C.int(index)))
}

func (s *Statement) ColumnDouble(index int) float64 {
	return float64(C.sqlite3_column_double(s.stmt, C.int(index)))
}

func (s *Statement) ColumnInt64(index int) int64 {
	return int64(C.sqlite3_column_int64(s.stmt, C.int(index)))
}

func (s *Statement) ColumnText(index int) string {
	n := C.int(s.ColumnSize(index))
	return C.GoStringN((*C.char)(unsafe.Pointer(C.sqlite3_column_text(s.stmt, C.int(index)))), n)
}
