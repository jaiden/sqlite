package sqlite

// #cgo pkg-config: sqlite3
//
// #include <stdlib.h>
// #include <stdint.h>
//
// int sqlite3_initialize(void);
// int sqlite3_threadsafe(void);
import "C"

func init() {
	C.sqlite3_initialize()

	if C.sqlite3_threadsafe() == 0 {
		panic("sqlite is compiled as thread unsafe")
	}
}
