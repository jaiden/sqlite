package sqlite

import (
	"testing"
)

func TestSimple(t *testing.T) {
	db, err := OpenDatabase(":memory:", OpenReadWrite)
	if err != nil {
		t.Fatalf("%q", err)
	}

	defer db.Close()

	if db.db == nil {
		t.Fatalf("%q", ErrUnknown)
	}
}
