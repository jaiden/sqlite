package sqlite

import (
	"testing"
)

func testInsert(t *testing.T, db *Database, id int64, name, pronoun string) {
	stmt, err := db.Prepare("INSERT INTO members VALUES(?, ?, ?)", 0x00)
	if err != nil {
		t.Errorf("%q", err)
	}

	if err := stmt.BindInt64(1, id); err != nil {
		t.Errorf("%q", err)
	}

	if err := stmt.BindText(2, name); err != nil {
		t.Errorf("%q", err)
	}

	if err := stmt.BindText(3, pronoun); err != nil {
		t.Errorf("%q", err)
	}

	rowReturned, err := stmt.Step()
	if err != nil {
		t.Errorf("%q", err)
	} else if rowReturned {
		t.Error("row returned in create table statement")
	}

	stmt.ClearBindings()
	stmt.Reset()
}

func testSelect(t *testing.T, db *Database, name, pronoun string) {
	stmt, err := db.Prepare("SELECT pronoun FROM members WHERE name = ?", 0x00)
	if err != nil {
		t.Errorf("%q", err)
	}

	if err := stmt.BindText(1, name); err != nil {
		t.Errorf("%q", err)
	}

	rowReturned, err := stmt.Step()
	if err != nil {
		t.Errorf("%q", err)
	} else if !rowReturned {
		t.Error("no row returned in select statement")
	}

	if stmt.ColumnText(0) != pronoun {
		t.Errorf("expected %s, got %s", pronoun, stmt.ColumnText(0))
	}

	stmt.ClearBindings()
	stmt.Reset()

}

func TestStatement(t *testing.T) {
	db, err := OpenDatabase(":memory:", OpenReadWrite)
	if err != nil {
		t.Fatalf("%q", err)
	}

	defer db.Close()

	if err := db.ExecuteTransient("CREATE TABLE IF NOT EXISTS members(id, name, pronoun)"); err != nil {
		t.Errorf("%q", err)
	}

	testInsert(t, db, 1, "jo", "he/him")

	testSelect(t, db, "jo", "he/him")

}
