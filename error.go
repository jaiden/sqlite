package sqlite

import (
	"C"
	"errors"
)

// Standard [result codes] for SQLite.
//
// [result codes]: https://www.sqlite.org/c3ref/c_abort.html
const (
	ResultOk         = 0
	ResultError      = 1
	ResultInternal   = 2
	ResultPerm       = 3
	ResultAbort      = 4
	ResultBusy       = 5
	ResultLocked     = 6
	ResultNomem      = 7
	ResultReadonly   = 8
	ResultInterrupt  = 9
	ResultIoerr      = 10
	ResultCorrupt    = 11
	ResultNotfound   = 12
	ResultFull       = 13
	ResultCantopen   = 14
	ResultProtocol   = 15
	ResultEmpty      = 16
	ResultSchema     = 17
	ResultToobig     = 18
	ResultConstraint = 19
	ResultMismatch   = 20
	ResultMisuse     = 21
	ResultNolfs      = 22
	ResultAuth       = 23
	ResultFormat     = 24
	ResultRange      = 25
	ResultNotadb     = 26
	ResultNotice     = 27
	ResultWarning    = 28
	ResultRow        = 100
	ResultDone       = 101
)

var (
	ErrUnknown = errors.New("unknown database error")

	ErrBusy        = errors.New("write busy")
	ErrDbCorrupted = errors.New("database has been corrupted")
	ErrInterrupted = errors.New("operation was interrupted")
	ErrOutOfRange  = errors.New("index out of range")
)

func resultToErr(r C.int) error {
	switch r {
	case ResultOk:
		return nil
	case ResultError:
		return ErrUnknown
	case ResultAbort:
		return ErrInterrupted
	case ResultBusy:
		return ErrBusy
	case ResultCorrupt:
		return ErrDbCorrupted
	}

	return ErrUnknown
}
