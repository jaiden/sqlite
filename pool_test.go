package sqlite

import (
	"sync"
	"testing"
)

func testPoolInsert(t *testing.T, pool *Pool, id int64, name, pronoun string) {
	db := pool.GetWrite(nil)
	if db == nil {
		t.Error("got empty write")
	}

	defer pool.PutWrite(db)

	testInsert(t, db, id, name, pronoun)
}

func testPoolSelect(t *testing.T, pool *Pool, name, pronoun string) {
	db := pool.GetRead(nil)
	if db == nil {
		t.Error("got empty read")
	}

	defer pool.PutRead(db)

	testSelect(t, db, name, pronoun)
}

func TestPool(t *testing.T) {
	pool, err := OpenPool("/tmp/cago.db", 4)
	if err != nil {
		t.Fatalf("%q", err)
	}

	defer pool.Close()

	db := pool.GetWrite(nil)
	if db == nil {
		t.Error("empty write")
	}

	if err := db.ExecuteTransient("CREATE TABLE IF NOT EXISTS members(id, name, pronoun)"); err != nil {
		t.Errorf("%q", err)
	}

	pool.PutWrite(db)

	testPoolInsert(t, pool, 1, "jaiden", "she/her")
	testPoolInsert(t, pool, 2, "jo", "he/him")

	var wg sync.WaitGroup

	wg.Add(4)
	go func() {
		testPoolSelect(t, pool, "jo", "he/him")
		wg.Done()
	}()
	go func() {
		testPoolSelect(t, pool, "jaiden", "she/her")
		wg.Done()
	}()
	go func() {
		testPoolSelect(t, pool, "jaiden", "she/her")
		wg.Done()
	}()
	go func() {
		testPoolSelect(t, pool, "jo", "he/him")
		wg.Done()
	}()
	wg.Wait()
}
